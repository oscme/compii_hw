#include <stdio.h>
#include <stdlib.h>
#include "token.h"
#include "Token.cpp"

string input = "";
int global_len = 0;
int contador = 0;
string character = '';

void setInput(text){
    input = text;
    global_len = input.size();
    contador = 0;
}

char getNextCharacter(){
    if (contador < global_len) {
        character = input[contador];
        contador += 1;
        return character;
    }    
    return '\0';
}

Token getNextToken(){
    character = getNextCharacter();
    while character == ' ' || character == '\n'{
    character = getNextCharacter();
    if (character == '\0')
        return Token(character, 999);
    if (character.isdigit())
        return Token(character, 107);
    if (character == '+')
        return Token(character, 101);
    if (character == '/')
        return Token(character, 104);
    if (character == '*')
        return Token(character, 103);
    if (character == '(')
        return Token(character, 105);
    if (character == ')')
        return Token(character, 106);
    }     
}